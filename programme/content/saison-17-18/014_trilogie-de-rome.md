<div class="push"></div>
<div class="article" markdown=true>

# Trilogie {: .stroke12}
# de Rome {: .stroke12}

## Ludovic Drouet

Création théâtre <br>
du lundi 23 au vendredi 27 avril 2018 à 20h30
{: .informations}

Qu’est devenue l’antiquité sinon une surface de projection ? L’histoire d’un crime qui s’est trouvé une scène.
Nous sommes dans les traces d’un continent englouti: le XXI<sup>e</sup> siècle. Un monde où le connu est devenu un horizon permanent. Que faire de la question que ce monde pose aux nouveaux fils de Rome? Qui fonde les empires? Le croyant ou l’athée? Si **Trilogie de Rome** n’est pas un mythe fondateur, elle voudrait en être l’IRM. Carambolage d’êtres et de systèmes politiques à la suite de quoi l’Empire est devenu — et pour quelques siècles — un horizon indépassable.
{: .intro}

> «Peut-être avons-nous passé l’âge d’écrire sous différents modes les préludes d’une même catastrophe et peut-être sommes-nous encore trop jeunes pour en narrer les séquelles. Notre ère est donc l’ère du désastre. Le temps n’y existe que pour être dilaté et c’est cette nouvelle capacité de dilatation qui le définit et nous définit.»
> <footer>Ludovic Drouet</footer>


Texte, mise en scène, costumes et conception scénographique
:   Ludovic Drouet

Avec
:   Didier De Neck,
:   Ludovic Drouet,
:   Lucas Meister,
:   Nicolas Patouraux

Œil extérieur
:   Julia Huet-Alberola

Création lumière
:   Iris Julienne

Création sonore
:   Noam Rzewski

Construction
:   Sébastien Corbière

Dresseur
:   Gaëtan Doppagne

Production déléguée
:   Théâtre la Balsamine

Une production de Ludovic Drouet en coproduction avec le Théâtre la Balsamine.
{: .production }

</div>
