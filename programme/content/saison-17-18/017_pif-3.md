<div class="article" markdown=true>

# PIF 3 - {: .stroke12}
# Pauvre et Informe {: .stroke12}
# Festival 3 {: .stroke12}

du jeudi 14 au samedi 16 juin 2018 à 20h30
{: .informations}

Le PIF, un festival devenu une institution aussi célèbre que le nez de Cléopâtre.
Troisième édition du festival ouvert à tous et à toutes les folies. Un moment de suspension et de fantaisie autour de formes pauvres et informes mais surtout indispensables à la diversité.
{: .intro}

Programmation
:   en cours

</div>
