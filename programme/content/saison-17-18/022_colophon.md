<!-- <div class="start-info"></div> -->
<div class="info-pratique" markdown=true>

# Partenaires  {: .exceptions-info}

La Balsamine est subventionnée par la Fédération Wallonie-Bruxelles et fait partie du réseau des Scènes chorégraphiques de la Commission Communautaire française de la Région de Bruxelles-Capitale.
{: .texteinfo}

La Balsamine reçoit aussi le soutien de Wallonie-Bruxelles Théâtre/Danse et de Wallonie-Bruxelles International.
{: .texteinfo}

###  

La Balsamine est signataire de la charte United Stages.
{: .texteinfo}

![alt](http://www.balsamine.be/images/logos/united-stages.svg)
{: .united}

Pour exprimer haut et fort son soutien aux populations civiles en danger un peu partout dans le monde, victimes de violences ou de toute autre forme de mise en danger, de nombreux partenaires du secteur culturel belge ont créé une charte éthique et le label UNITED STAGES. Le but est de rassembler et d’utiliser les forces vives des arts vivants et du monde associatif ainsi que la proximité qu’ils entretiennent avec leurs publics pour initier des actions concrètes porteuses d’un changement positif pour leurs bénéficiaires. Diverses actions vont être mises en place, à la Balsamine, comme dans d’autres lieux.
{: .texteinfo}

Une initiative de Action Sud CCR, Choux de Bruxelles, Globe Aroma, Kaaitheater, La Bellone, La Vénerie, La Tentation, Le boson, Les Midis de la poésie, MET-X, Passa Porta, Théâtre de l’Ancre, Théâtre la Balsamine, Théâtre Océan Nord, Théâtre Varia, Théâtre 140…
{: .texteinfo}

</div>
<div class="info-pratique" markdown=true>

# Équipe  {: .exceptions-info}

Direction générale et artistique
:   [Monica Gomes](mailto:monica.gomes@balsamine.be)  

Conseiller artistique
:   Fabien Dehasseler  

Direction financière et administrative
:   [Morgan Brunéa](mailto:morgan.brunea@balsamine.be)

Coordination générale, communication et accueil compagnies
:   [Fanny Arvieu](mailto:fanny.arvieu@balsamine.be)

Presse, promotion et relations publiques
:   [Irène Polimeridis](mailto:relations.publiques@balsamine.be)

Comédienne et romaniste - Médiation écoles et associations
:   [Noemi Tiberghien](mailto:noemi.tiberghien@balsamine.be)

Metteur en scène et auteur - Artiste associée
:   [Martine Wijckaert](mailto:martine.wijckaert@balsamine.be)

Résidence artistique et administrative
:   [Théâtre du Tilleul](http://www.theatredutilleul.be)

Direction technique
:   [Jef Philips](mailto:jef.philips@balsamine.be)

Régisseur
:   [Rémy Urbain](mailto:remy.urbain@balsamine.be)

Régisseur stagiaire
:   Brice Agnès

Responsable bar
:   Franck Crabbé


Photographe de plateau associé
:   Hichem Dahes

Designers graphique associés
:   [Open Source Publishing](http://osp.kitchen)

Artiste associée, visuels de la saison 17-18
:   [Julie Kern Donck](http://www.juliekerndonck.be)

</div>
<div class="info-pratique" markdown=true>

<div class="push"></div>

#Colophon  {: .exceptions-info}

**Outils et programmes:**  
html2print, inkscape, gimp, Blender, git, gitlab, etherpad,  GNU/Linux
{: .texteinfo}

**Typographies:**  
Ume stroke stroke condensed, Ume stroke stroke expended
{: .texteinfo}

Fichiers de mise en page disponibles sous licence art libre sur http://osp.kitchen /work/balsamine.2017-2018/
{: .texteinfo}

Fichiers images CC BY-SA-ND
{: .texteinfo}

**Impression:**  
Imprimerie Gillis, Bruxelles.
{: .texteinfo}

**Éditeur responsable:**  
Monica Gomes, <br>Avenue Félix Marchal, 1 <br>1030 Bruxelles
{: .texteinfo}

</div>
