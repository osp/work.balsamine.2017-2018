<!-- <div class="push"></div> -->
<div class="article" markdown=true>

# La lettre volée {: .stroke10}

## Denis Bosse /  Pascal Nottet /  Thomas Van Haeperen /  Frédéric Dussenne

Création Vidéopéra <br>
du jeudi 9 au samedi 11 novembre 2017 à 20h30
{: .informations }

En partenariat avec Ars Musica.
{: .subhead }

![alt](http://www.balsamine.be/images/logos/ars-musica.svg)
{: .logo}

![alt](../../images/show/regency-desk-2-transparent.png)
![alt](../../images/show/pen-hd.png)
![alt](../../images/show/letter.png)

Une enquête palpitante à la recherche d’une lettre perdue.
**La lettre volée**, opéra librement inspiré de la nouvelle éponyme d’Edgar Poe, convoque à la fois public, chanteurs et musiciens au cœur d’une action dramatique: la poursuite d’une lettre volée qui rend fous ceux qui la désirent.
Une vertigineuse, dramatique, musicale et existentielle mise en abîme.
{: .intro }

Directeur artistique et compositeur
:    Denis Bosse

Librettiste
:    Pascal Nottet et Denis Bosse

Directeur musical
:    Thomas Van Haeperen

Metteur en scène
:    Frédéric Dussenne

Chanteurs
:    Nicolas Ziélinski (Contre-ténor — Dupin),
:    Sarah Defrise (Soprano — Le fou),
:    Lorenzo Carola (Ténor — Le préfet de Police),
:    Shadi Torbey (Baryton Basse — Ministre),
:    Anne Matic (Mezzo Soprano — Reine),
:    Thomas Van Caekenberghe (Baryton — Narrateur)

Musiciens
:    Ensemble Sturm und Klang sous la direction de Thomas Van Haeperen avec la participation d’étudiants et professeurs d’ART²,
:    Maxime Stasyk (violon 1),
:    Loris Douyez (violon 2),
:    Dominica Eyckmans (alto),
:    Catherine Lebrun (violoncelle),
:    Natacha Save (contrebasse),
:    Justine Debeer (clarinette),
:    Amaury Geens (saxophones),
:    SzeFong Yeong (cor),
:    Jean-Louis Maton (percussions),
:    Olivier Douyez (accordéon),
:    Pierre Thomas (piano)

Arts Visuels
:    Étudiants d'ARTS²
:    Jean-François Octave (direction)

Scénographie et vidéo
:    Helga Dejaegher

Assistants scénographie et vidéo
:    Dimitri Baheux,
:    Emmanuel Selva

Une production Quart de Ton — Janine Al-Asswad . Avec les soutiens de :  Ars Musica, Théâtre la Balsamine, Fédération Wallonie-Bruxelles — conseil de la musique contemporaine, Sturm und Klang, Forum des compositeurs et Festival Loop . En collaboration avec ARTS², École Supérieure des Arts de Mons (Domaine Arts visuels — Musique et Théâtre).
{: .production }

</div>
