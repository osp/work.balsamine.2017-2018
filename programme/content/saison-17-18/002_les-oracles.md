<div class="push"></div>
<div class="article" markdown=true>


#Ouverture {: stroke12}

le jeudi 28 septembre
{: .informations}

dès 19h<br>
Vernissage exposition<br>
Julie Kern Donck, créatrice des visuels<br>
de la saison 17-18<br>
{: .informations}

Graveuse et sérigraphe de formation, Julie Kern Donck s'attache à explorer les techniques de représentation de l'image. Naviguant entre travail manuel et digital, son œuvre est traversée d'allers retours entre matériel et virtuel, entre rationalité et poésie. Pour la saison 2017-2018 de la Balsa, l'image digitale a quelque peu perdu le sens des réalités, comme des proportions...
{: .intro}

de 19h30 à 20h<br>
La minute impudente
{: .informations}

Les artistes de la saison partageront avec vous «&nbsp;une minute impudente&nbsp;».
{: .intro}


20h30
{: .informations .premiere}

Première du spectacle **Les Oracles**
{: .intro}

</div>
<div class="article" markdown=true>


# Les oracles {: .stroke12}

Création transversale <br> danse/​vidéo/​musique/​texte <br>
du jeudi 28 au samedi 30 septembre 2017 à 20h30
{: .informations}


![alt](http://www.balsamine.be/images/logos/transcultures.svg)
![alt](http://www.balsamine.be/images/logos/rhizome.svg)
{: .logo}

![alt](../../images/show/crystal-ball-1.png)
![alt](../../images/show/knuckebones.png)
![alt](../../images/show/knucklebone-tex.png)
![alt](../../images/show/les-oracles-creampot-1.png)  

**Les oracles** allient danse, poésie, création sonore et vidéo autour de thématiques à la fois féministes et intemporelles, traversées par les textes des écrivaines Catrine Godin et Martine Delvaux.
La question du genre dans nos perceptions et nos imaginaires contemporains est ici poétisée. Une proposition artistique belgo-québécoise.
{: .intro}


Conception, direction artistique et mise en scène
:    Simon Dumas

Coordination et administration du projet
:    Yves Doyon

Conception sonore et coproduction
:    Philippe Franck

Création vidéo
:    Thomas Israël

## Percées

Auteure
:    Catrine Godin

Chorégraphie
:    Karine Ledoyen

Interprètes (dans la vidéo)
:    Fabien Piché,
:    Ariane Voineau

## Prototype Nº1

Auteure
:    Martine Delvaux

Chorégraphie
:    Manon Oligny

Interprète et co-chorégraphe
:    Marilyn Daoust


Une production de Rhizome, coproduit par Transcultures, avec les soutiens de Manon fait de la danse, du Théâtre la Balsamine, du Conseil des Arts du Canada, du Conseil des arts et des lettres du Québec, du ministère des Relations internationales et Francophonie du Québec et de WBI.
{: .production }

</div>
