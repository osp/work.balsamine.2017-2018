<div class="push"></div>
<div class="article" markdown=true>

# Scripta Manent {: .stroke12}

Cycle nouvelles écritures <br>
du jeudi 14 au vendredi 15 décembre 2017 à 20h30
{: .informations}

De nouvelles façons de voir le monde et de concevoir l’acte d’écrire. Textes, processus narratifs, l’occasion d’apercevoir de nouveaux visages, d’entendre et de voir des liens qui se nouent, des lianes qui s’entrelacent les unes aux autres afin de les faire vivre. L'écriture, au sens large.
{:. intro}

Programmation
:    en cours

</div>
