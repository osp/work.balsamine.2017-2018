<div class="article" markdown=true>

# Should I stay or {: .stroke10}
# should I stay {: .stroke10}

## Simon Thomas

Reprise Théâtre <br>
le jeudi 16 novembre 2017 à 20h30 <br>
le lundi 20 novembre 2017 à 20h30 <br>
le mercredi 22 novembre 2017 à 20h30 <br>
le jeudi 23 novembre 2017 à 20h30 <br>
le vendredi 24 novembre 2017 à 20h30
{: .informations}


![alt](../../images/show/cat-bake-tex-split-3.png)
![alt](../../images/show/chat-3-queues.png)
![alt](../../images/show/cat-bake-tex-split-1.png)
![alt](../../images/show/chat-3-queues-transparent.png)
![alt](../../images/show/sis2.jpg)
![alt](../../images/show/sis3.jpg)
![alt](../../images/show/sis4.jpg)
![alt](../../images/show/sis5.jpg)
![alt](../../images/show/sis6.jpg)
![alt](../../images/show/sis8.jpg)


Les quatre fantastiques reviennent et reprennent leur rengaine : Should I…
Ils sont quatre, ils sont enfermés dans une pièce dont les portes ne sont pas verrouillées et pourtant ils ne peuvent pas sortir. Un casse-tête bien surréaliste et qui nous pose des questions très existentialistes.
Qui sont-ils, d’abord? Comment les appelle-t-on? Gabor? Paola? Héraclès? Falbala? Téflon? Gargan? Lazlo? Axel? Cyrus? Mia? Edmée? Augustine?
Où sont-ils coincés? Près d’un lac? Dans une fissure du temps? Sur une scène? Dans une autre dimension?
Ils parlent de mort, d’exil sur Mars, d'accès à la vie éternelle, du temps qui passe, d'enlisement, de violence gratuite… Peu à peu, un système se met en place entraînant les protagonistes dans un trou noir. La suite des évènements, l’alignement des planètes, le hasard, et même la chance sont pour beaucoup dans la résolution de leur impasse. Les carottes sont-elles cuites ou pas? À revoir, donc, puisqu’il s’agit d’une reprise. Non? Mais oui, mais oui.
{: .intro}

Écriture et mise en scène
:    Simon Thomas

Avec
:    Jules Churin,
:    Héloïse Jadoul,
:    Manon Joannotéguy,
:    Lucas Meister

Costumes
:    Camille Flahaux

<div class="push"></div>

Premières élaborations au sein de l'Institut National Supérieur des Arts du Spectacle et des Techniques de Diffusion de la Fédération Wallonie-Bruxelles. Réalisé avec les soutiens du Théâtre la Balsamine, avec l'aide de la Fédération Wallonie-Bruxelles — Service du Théâtre et le soutien du Fonds Marie-Paule Delvaux Godenne, géré par la Fondation Roi Baudouin.
{: .production }

</div>
