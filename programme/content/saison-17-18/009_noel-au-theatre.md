<div class="article" markdown=true>

# Noël au Théâtre {: .stroke12}

du mardi 26 au samedi 30 décembre 2017
{: .informations}

![alt](http://www.balsamine.be/images/logos/ctej.svg)
{: .logo}

La création « jeune public » est dans tous ses états à l’occasion du Festival Noël au Théâtre : théâtre de textes, de marionnettes, d'objets, d'ombre, de musique, de danse.  
Cette saison encore, la Balsamine accueille le festival et ouvre grandes ses portes à la magie et la chaleur du théâtre pour tous.
{: .intro}

</div>
