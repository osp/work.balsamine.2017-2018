<div class="article" markdown=true>

# On pense {: .stroke12}
# à vous {: .stroke12}

## Théâtre de Galafronie

Théâtre pour tous à partir de 6 ans <br>
le vendredi 4 mai 2018 à 20h <br>
le samedi 5 mai 2018 à 18h
{: .informations}

![alt](../../images/show/enamel-teapot-corrected-gamma.png)
![alt](../../images/show/carpette-mongole-tex-1.png)

En 2018, le Théâtre de Galafronie aura 40 ans et tire sa révérence. On saisit cette dernière chance d’accueillir **On pense à vous** pour vous permettre de voir ou revoir ce très beau spectacle qui tourne depuis dix ans. Ne ratez pas la Dame de la Yourte, elle est de retour, une dernière fois!
{: .intro}

> «Elle nous accueille chez elle, dans son nid: une yourte nomade, ronde et chaleureuse où la vie circule librement, sans se blesser aux angles de la bienséance.
>
> Avec ses pinceaux et ses crayons, avec l’encre, l’eau et les couleurs, elle fait surgir ceux qui habitent sa tête et son cœur. Ce spectacle qui mêle théâtre et peinture nous emporte dans les méandres de la vie et de la création artistique, là où il n’est pas possible de tricher ni avec soi-même, ni avec l’autre.
>
> Touchant, poétique, ludique et insoumis, ce spectacle transforme le regard, rend fort parce qu’il assume la fragilité, la subjectivité du regard.
>
> Un grand moment de théâtre et d’humanité.»
>
> <footer>Jeanne Pigeon, fondatrice de la Montagne Magique</footer>

Texte et interprétation
:   Marianne Hansé

Mise en scène
:   Didier de Neck

Aide à la recherche et régie
:   Valère Le Dourner

Création lumière et régie
:   Guy Carbonnelle

Une production du Théâtre de Galafronie. Avec le soutien de la Fédération Wallonie-Bruxelles — Service du théâtre.
{: .production}

</div>
