<div class="article" markdown=true>

# Les mardis {: .stroke12}
# contemporains {: .contemporain}
  
le mardi 3 octobre 2017 à 20h30 <br>
le mardi 14 novembre 2017 à 20h30 <br>
le mardi 21 novembre 2017 à 20h30
{: .informations}

En partenariat avec Ars Musica
{: .subhead}

![alt](http://www.balsamine.be/images/logos/ars-musica.svg)
{: .logo}

![alt](../../images/show/Visuel_ars-musica.jpg)

**Les mardis contemporains** proposent aux oreilles curieuses de découvrir des facettes de la musique d’aujourd’hui, dans un programme pluriel qui dialogue avec l’histoire, la philosophie, la sociologie, mais aussi le cirque, le cinéma, la science ou l’architecture…
Le temps de 3 concerts-rencontres, **les mardis contemporains** prennent leurs quartiers à la Balsamine.
{: .intro}

* * *

### Octobre rouge

#### Le 3 octobre à 20h30

Invité
:    Jean-Jacques Marie (historien, auteur de Lénine, la révolution permanente, Ed. Payot)

Œuvres
:    Elégie et Polka de Dimitri Chostakovitch (1931),
:    Quatuor à cordes N°1 de Dimitri Kabalevski — extrait (1928),
:    Quatuor N°3 d’Arthur Lourié — extrait (1926),
:    Quatuor N°3 de Nikolai Roslavets (1920),
:    Trio à cordes d’Edison Denisov (1969),
:    Création pour quatuor à cordes de Alice Hebborn (commande Ars Musica 2017)

Interprétation
:    Quatuor Amôn

* * *

### Architecture

#### Le 14 novembre à 20h30

Invités
:    Francis Metzger et François Schuiten

Œuvres
:    Tetras de Iannis Xenakis (1983),
:    Quatuor à cordes n°2 de Luis Naon (extrait du cycle Urbana 1999–2001),
:    City Life de Steve Reich (1995),
:    Création de Stéphane Orlando (commande Ars Musica 2017)

Direction
:    Thomas Van Haeperen

Interprétation
:    Quatuor TANA / Ensemble Sturm & Klang

* * *

### Cirque

#### Le 21 novembre à 20h30

Invités
:     Julien Rosemberg, Directeur Général adjoint — Pôle National Cirque et Arts de la rue

Œuvres
:    Sonate n°2 pour violoncelle de Gyorgy Ligeti (1953),
:    Circus Polka d’Igor Stravinsky (1942),
:    March for Piano: The Circus Band de Charles Ives (1898-1899),
:    Duration III de Morton Feldman (1961),
:    Extrait de Parade de Eric Satie (1916-1917),
:    Création de Stefan Hejdrowski (commande Ars Musica 2017)

Interprétation
:    Guy Danel ( violoncelle),
:    Gabi Sultana et François Mardirossian ( piano),
:    Elsa Bouchez et Philippe Droz ( artistes de cirque)

</div>
