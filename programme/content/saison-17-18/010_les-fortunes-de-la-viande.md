<div class="push"></div>
<div class="article" markdown=true>

# Les fortunes {: .fortunes}
# de la viande {: .stroke10}

## Martine Wijckaert

Création théâtre <br>
du mardi 30 janvier au samedi 10 février 2018 à 20h30 (relâche le dimanche)
{: .informations}

![alt](../../images/show/agneau-wireframe.png)
![alt](../../images/show/SheepDiffuseSkin-Contours.png)
![alt](../../images/show/sheep-helpers.png)

Un aller simple pour le trou du cul du monde. Bienvenue dans l’anus planétaire.
**Les fortunes de la viande** narrent les tribulations existentielles d’un boucher-charcutier, de sa femme et du confesseur de celle-ci, que rejoint à l’impromptu Diane Chasseresse, déboulée pour ainsi dire dans ce marasme métaphysico-trivial. Au fil de ce texte épique, où s’interpénètrent épisodes quotidiens des plus triviaux et mouvements de l’âme parfaitement irrationnels, c’est la métaphysique obscure du quotidien qui est en action. Car les personnages aspirent invariablement au chaos, mus par une volonté d’incorrection absolue visant à briser tous les canons sociétaux.
{: .intro }

Avec
:   Marie Bos,
:   Héloïse Jadoul,
:   Claude Schmitz,
:   Alexandre Trocki

Écriture et mise en scène
:   Martine Wijckaert

Assistante à la mise en scène
:   Astrid Howard

Scénographie
:   Valérie Jung

Lumières
:   Stéphanie Daniel

Costumes et accessoires
:   Laurence Villerot

Image vidéo
:   Jacques André

Création son
:   Thomas Turine

Direction technique
:   Fred Op de Beeck

Régie spectacle
:   Mathieu Bastyns

Une production du Théâtre la Balsamine en coproduction avec La Coop asbl, avec le soutien du tax-shelter du gouvernement fédéral belge.
{: .production }

</div>
