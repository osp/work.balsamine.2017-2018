<div class="start-info"></div>
<div class="info-pratique" markdown=true>

# Billetterie

Tarif mécène
{: .tarif}

20€
{: .prix}

Être mécène, c’est participer à la pérennité des fondamentaux de la Balsamine que sont l’échange et le dialogue, la création et l’enjeu artistique et expérimental.
C’est nous aider à soutenir la création artistique. C’est reconnaître et accompagner une politique qui soutient les jeunes artistes.
C’est vous associer au pari de la jeunesse et de la rencontre des savoirs issus de différentes disciplines.
C’est une opportunité singulière de nous permettre de poursuivre une politique tarifaire accessible à tous.
{: .footnote }


Tarif plein
{: .tarif}

15€
{: .prix}

Prévente 12€
{: .center}


Tarif réduit<sup>*</sup>
{: .tarif}

10€
{: .prix}

Prévente 7€ <br>
{: .center}


Tarif enfant
{: .tarif}

(- de 12 ans)
{: .center}

5€
{: .prix}


Tarif étudiants
{: .tarif}

en écoles supérieures artistiques
{: .center}

5€
{: .prix}


Tarif Mardis contemporains
{: .tarif}

12€
{: .prix}

Tarif réduit<sup>*</sup> 10€
{: .center}

Article 27 <br>et Arsène 50
{: .tarif}

*Étudiants, + de 60 ans, demandeurs d’emploi, professionnels du spectacle, schaerbeekois
{: .footnote .reduit}

<div class="push"></div>

Tarifs <br>  de groupe
{: .tarif}

(à partir de 10 personnes)
{: .center}

Groupe adulte
{: .groupes}

10€ la place
{: .prix}

1 place offerte à l'organisateur
{: .center}

Groupe étudiant
{: .groupes}

6€ la place
{: .prix}

1 place offerte à l'enseignant
{: .center}


Le paiement des réservations de groupe doit s’effectuer 10 jours avant la date de représentation choisie
{: .footnote }


## United Stages

Au sein du Label United Stages, la Balsamine réalise une grande collecte de fonds. Le premier bénéficiaire est : Refugees welcome, plateforme citoyenne de soutien aux réfugiés.
{: .texteinfo}

Vous pouvez être solidaire de cette initiative à tout moment de la saison. Il vous suffit d’ajouter le montant que vous souhaitez verser à celui des places achetées. La récolte de fonds sera intégralement reversée.
{: .texteinfo}

Plus d’infos sur United Stages page 12-13.
{: .texteinfo}

## Le Pass

Et si vous tentiez le pass…
Un pass nominatif au prix de 20 euros et qui vous donne accès à huit spectacles de la saison.
La réservation des places peut se faire à tout moment, sous réserve des places disponibles.
Intéressé ? Contactez notre bureau des réservations.
Attention, le pass est vendu jusqu'au 30 septembre. Le nombre de pass mis en vente est limité.
{: .texteinfo}

![pass balsamine](../../images/PASS.png){: .pass}




## Réservations

### **Par téléphone**

Le bureau des réservations est joignable au  +**32 2 735 64 68**. Du lundi au vendredi, de 14 h à 18 h, la Balsamine prend vos réservations ou vous donne des renseignements complémentaires. En dehors de ces heures et le week-end, un répondeur prend vos réservations.
{: .texteinfo}

### **Par e‑mail**

En envoyant un e‑mail à l’adresse** reservation@balsamine.be** en précisant votre nom et votre prénom, le nom du spectacle, la date de représentation, le(s) tarif(s) dont vous bénéficiez et le nombre de places souhaitées. Un e‑mail de confirmation vous sera ensuite renvoyé.
{: .texteinfo}

### **Par le site internet**

Accessible 24h/24, vous pouvez cliquer sur le lien « réserver » présent sur chaque page de spectacle du site et compléter le formulaire. Un e‑mail de confirmation vous sera ensuite envoyé. Cette réservation en ligne concerne à la fois les places au tarif prévente mais aussi les places qui ne seront payées que le soir de votre venue.
{: .texteinfo}

## Moyens de paiement <br>et retrait des places

### **Préventes**

Pour bénéficier du tarif prévente, le paiement doit avoir lieu 48h avant la date de représentation choisie.
Par virement bancaire au numéro de compte suivant :
{: .texteinfo}

 IBAN&nbsp;BE15&nbsp;0680&nbsp;6267&nbsp;2030 — BIC&nbsp;GKCCBEBB
{: .texteinfo}

Nous vous remercions d’indiquer en communication votre nom, votre prénom, la date de représentation choisie et la communication structurée qui vous a été transmise soit par e‑mail, soit par téléphone.
{: .texteinfo}

### **Sur place**

La billetterie est ouverte les soirs de représentation à partir de 19h.
{: .texteinfo}

</div>
