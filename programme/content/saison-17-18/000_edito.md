<div class="edito" markdown=true>


#En toute impudence

Et voici que vient 17-18.
{: .txtedito1}

Une saison qui  s’est construite en toute impudence, en toute imprudence. Un vent de passion, un soupçon d’effronterie.
{: .txtedito1}


Les artistes ont le courage d’une folie qui fait résister. Ils assument, portent une parole et débattent pour l’avenir. Aujourd’hui, la Balsamine devient plus que jamais, un lieu de solidarité où la culture se vit ensemble et se partage. Artistes et spectateurs s’unissent pour porter un projet commun et bataillent pour une culture du non-marchand.
{: .txtedito1}


Appel à la vivacité, à l’effronterie du rêve, de l’imaginaire, de la pensée vagabonde. Appel à l’insolence de l’exigence, de l’engagement.
{: .txtedito1}


Foncez, spontanément, dans cette saison culottée qui ne requiert aucun code à intégrer pour bien se conduire.
{: .txtedito1}


## Un projet éthique
## pour les 5 saisons à venir.

Une Balsa à la mine épurée, apurée, en purée, certains soirs et selon les menus. Pur jus, pur théâtre. En l’état.
{: .txtedito2}


**Partagé**, parce qu’ouvert sur le monde, prêt à recevoir l’autre dans sa
diversité, dans sa complexité. En dialogue, car c’est la seule voie de
l’apaisement.
{: .txtedito2}


Le seul chemin possible, c’est l’accessibilité à tous, même aux abeilles qui
peuplent notre toit. La Balsa est un lieu qui change et grandit avec les
artistes qui l’habitent, avec les publics qui la traversent. La Balsa mûrit et
contamine par son esprit libertaire. Nous sommes une terre pour les langues
étranges et réfléchissantes, un endroit où l’on peut se perdre et mieux se
retrouver.
{: .txtedito2}


**Utopique**, quoi de plus logique. Le risque de voir, le risque de faire, le
risque de dire et d’entendre, c’est ça l’utopie : offrir un développement à des
œuvres singulières. Une scène de toutes les aventures. Un lieu qui risque car
c’est vivre plus et il n’y a pas de risque sans l’humain qui le prend, qui le
court. Des créations qui mettent en scène les narrations que notre époque
fabrique. Des narrations fragmentaires, hybrides. Leur donner du temps, de
l’espace.
{: .txtedito2 .plus-bas}


**Responsable**, enfin. C’est une maison verte, qui tente par ses choix de
respecter les hommes et la nature, de vivre dans un développement durable et
d’investir ses moyens dans tout ce que la créativité a de plus riche. Hier,
aujourd’hui et demain.
{: .txtedito2}

<footer>Monica Gomes, direction générale et artistique de la Balsamine.</footer>

</div>
