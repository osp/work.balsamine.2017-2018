<div class="push"></div>

<div class="article" markdown=true>

# Etna {: .stroke12}

## Thi-Mai Nguyen

Création danse <br>
du mercredi 7 au jeudi 8 mars 2018 à 20h30
{: .informations }

Dans le cadre de Brussels, dance ! Focus on contemporary dance.
{: .subhead }

![alt](http://www.balsamine.be/images/logos/brussels-dance.svg)
{: .logo}

![alt](../../images/show/etna-tex.png)
![bag](../../images/show/etna-bag.png)
![alt](../../images/show/etna-bake-tex-2.png)


Une femme errante et solitaire, tel un spectre hanté par ses débris de vie. **Etna** est une femme sans âge, sans domicile, harcelée par les sonorités de sa vie passée et dont le corps incarne une lassitude extrême. Elle squatte la scène du théâtre et l’habite au travers de ses obsessions et de ses souvenirs.
{: .intro }

> «La pauvreté est là, au-dessus de nos têtes, comme une épée de Damoclès. Et nous poursuivons notre route sans penser que cela nous concerne. La frontière est mince entre la raison et l’égarement.
> Un système à bout de souffle. Un vortex qui engloutit tout et ne laisse aucun espoir. Des hommes et des femmes, au bord du gouffre, sans utopies. Ils sont le miroir d’une société en déclin, d’une société qui ne respire plus. D’un monde qui s’étouffe dans son individualisme.
> Et pourquoi tant de gens sont happés dans ce trou, dans ce retranchement. Cela tient à peu. Une perte d’emploi, une perte amoureuse. Une perte qui mène à la dérive et les fait sortir de notre réalité.
> En soi, personne n’est à l’abri d’une telle chute, nous sommes tous des funambules et nous résistons comme nous le pouvons. Nous luttons contre la folie qui nous guette.
> C’est de cela que je veux parler, de cette absurdité, de ce monde « presque » invisible qui inonde nos rues.»
> <footer>Thi-Mai Nguyen</footer>


Conception et interprétation
:   Thi-Mai Nguyen

Création lumières
: Rémy Urbain

Une production de Thi-Mai Nguyen avec les soutiens du Théâtre la Balsamine et
l'aide de la Maison de la création, Ultima Vez et du Théâtre Marni.
{: .production }

</div>
