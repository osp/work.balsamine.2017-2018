<div class="article" markdown=true>

# Les loups {: .stroke12}

## Jean Le Peltier

Création théâtre <br>
du lundi 26 au vendredi 30 mars 2018 à 20h30
{: .informations }

![alt](../../images/show/paramecie1.png)
![alt](../../images/show/../../images/show/compo4.png)
![alt](../../images/show/../../images/show/tex_0_transparent.png)

Une histoire étonnante sur la solidarité entre les espèces, sur la survie en milieu hostile.
Trois biologistes en Antarctique, éloignés de leur campement afin d’élargir leur champ de recherche, se perdent physiquement et mentalement. L’espace du désert blanc révèle soudainement leur animalité enfouie, leur esprit de meute refoulé.
C’est dans cette urgence, sous le joug de la survie que **Les Loups** nous parlent de leur choix. Des choix qui impliquent des orientations, des stratégies, des questions éthiques, des renoncements et qui témoignent de notre étourdissement devant le vide.
{: .intro }

Texte et mise en scène
:   Jean Le Peltier

Avec
:   Pierrick De Luca,
:   David Koczij,
:   Jean Le Peltier,
:   Cécile Maidon

Scénographie
:   Vincent Glowinsky

Lumière
:   Émily Brassier

Costumes
:   Agathe Thomas

Diffusion
:   Entropie

Production déléguée
:   Théâtre la Balsamine en collaboration avec Entropie Production

Une production Ives & Pony en coproduction avec le Théâtre la Balsamine et Le Vivat — Scène conventionnée Danse & Théâtre — Armentières.
{: .production }

</div>
