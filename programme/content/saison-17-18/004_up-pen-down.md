<div class="article" markdown=true>

# Up Pen Down {: .stroke12}

## OSP /  Adva Zakaï

Danse codée et commentée <br>
le samedi 14 octobre 2017 à 20h30
{: .informations}

Dans le cadre de la Saison des Cultures Numériques 2017
{: .subhead}

![alt](http://www.balsamine.be/images/logos/saison-numerique.svg)
{: .logo}

Les typographes d’[Open Source Publishing](http://osp.kitchen/) et la chorégraphe Adva ZakaÏ reviennent avec un nouvel épisode de leur recherche entre corps et écriture — danse et typographie qui oscille entre des
moments de type conférence, de prototypage de mouvement, de monstration de processus logiciels et de mouvements de traceurs (plotters).
{: .intro}

Distribution
:    en cours

Avec les soutiens de : la Fédération Wallonie-Bruxelles – Arts numérique, la Vlaamse Gemeenschapscommissie et le Théâtre la Balsamine.
{: .production}

</div>
