<div class="push"></div>

<div class="article" markdown=true>

# i-clit {: .stroke12}

## Mercedes Dassy

Création danse <br>
du mardi 27 février au samedi 3 mars 2018 à 20h30
{: .informations }


Dans le cadre de Brussels, dance! Focus on contemporary dance.
{: .subhead }

![alt](http://www.balsamine.be/images/logos/brussels-dance.svg)
{: .logo}

![alt](../../images/show/shoes-pillows-hd.png)
![alt](../../images/show/i-clit-composition-gamma-ok.png)

Un spectacle manifeste du corps, de la chair et où l'objet sexuel devient sujet.
**i-clit** tente un bilan chorégraphié de l’état du féminisme contemporain.
Une nouvelle vague féministe est née — ultra-connectée, ultra-sexuée et plus populaire. Mais face au pouvoir ambivalent de la pop culture, où et comment se placer dans ce combat en tant que jeune femme? Quelles armes utiliser?
**i-clit** traque ces moments de fragilité, où l’on glisse facilement de l’affranchissement à une nouvelle forme d’oppression.
{: .intro }

Concept, chorégraphie, interprétation
:   Mercedes Dassy

Dramaturgie, regard extérieur
:   Sabine Cmelniski

Création lumière
:   Caroline Mathieu

Costumes, scénographie
:   Justine Denos, Mercedes Dassy

Création sonore
:   Clément Braive

Diffusion
: Art Management Agency (AMA)

Production déléguée
: Théâtre la Balsamine


Avec les soutiens de la Fédération Wallonie-Bruxelles -  Service de la Danse, du Théâtre Océan Nord, de l’Escaut, du B.A.M.P., de Project(ion) Room et de Friends with benefits. Le son de ce spectacle a été créé grâce au soutien de la SACD. 
{: .production }

</div>
