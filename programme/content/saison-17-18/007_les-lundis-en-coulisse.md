<div class="article" markdown=true>

# Les lundis {: .stroke12}
# en coulisse {: .coulisse}

À la rencontre des écritures dramatiques contemporaines <br>
le lundi 11 décembre 2017 de 14h à 18h
{: .informations}

![alt](http://www.balsamine.be/images/logos/lundis.svg)
{: .logo}


**Les lundis en coulisse** sont un moment de rencontre informel permettant de découvrir à chaque séance des textes dramatiques contemporains qui n'ont jamais été présentés, et qui sont les coups de cœur d'un invité, dit “Le Passeur".
Les rôles des pièces sont distribués au cours de la séance à ceux qui ont envie de s'aventurer dans une “lecture découverte" à voix haute, tandis que d'autres écoutent.
**Les lundis en coulisse** belges se sont inspirés d’un dispositif du même nom, inventé par la metteure en scène **Gislaine Drahy**, et qui existe depuis 2002 à Lyon. Depuis il a été repris par **François Rancillac** au Théâtre de l’Aquarium à Paris ainsi que par la Compagnie Les encombrants à Dijon. Nous remercions Gislaine Drahy de nous avoir permis l’organisation des Lundis en Belgique. Ici **Les lundis en coulisse** sont une initiative de **Silvia Berutti-Ronelt**.
{: .intro }

Une coproduction de
:   Atelier Théâtre Jean Vilar, Centre d’études théâtrales de l’UCL, ARTS² – Conservatoire de Mons, La Bellone, Rideau de Bruxelles, Théâtre la Balsamine, Théâtre de Liège, Théâtre des Martyrs, Théâtre Varia, SACD  en partenariat avec le Centre des Écritures Dramatiques – Wallonie Bruxelles.

</div>
