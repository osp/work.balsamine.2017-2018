<div class="start-saison"></div>
<div class="article" markdown=true>

#L’École des {: .stroke12}
# Maîtres 2017 {: .maitre}

## XXVI<sup>e</sup> édition Transquinquennal

Théâtre <br>
le lundi 11 septembre 2017 à 20h
{: .informations}

Entrée libre mais réservation conseillée&nbsp;!
{: .subhead}

![logo](http://www.balsamine.be/images/logos/ecole-maitres.svg)
{: .logo}

![alambic](../../images/show/alambic-3.png)
![calculette](../../images/show/calculette.png)
![banknote](../../images/show/banknote-bake-tex.png)

On retourne à l’école avec Transquin, maître quinquin&nbsp;!
**L'École des Maîtres** est une académie théâtrale européenne itinérante, regroupant des comédiens venus de Belgique, d'Italie, de France et du Portugal. Quinze d’entre eux se retrouveront sous le regard du collectif Transquinquennal passé maître dans l’art de questionner nos identités.
Ensemble, ils se pencheront sur la marchandisation de nos vies et de notre quotidien. La leçon promet d’être passionnante.
{: .intro}

Une organisation du CREPA.
{: .production}

Partenaires du projet et direction artistique: CSS — Teatro stabile di innovazione del Friuli Venezia Giulia (Italie);
CREPA — Centre de Recherche et d'Expérimentation en Pédagogie Artistique
(CFWB/Belgique); TAGV — Teatro Académico de Gil Vicente, Colectivo 84 (Portugal); La Comédie de Reims — Centre Dramatique National et La Comédie de Caen — Centre Dramatique National (France)
{: .production}

Avec le soutien de MIBACT — Direzione Generale Spettacolo dal vivo (Italie).
{: .production}

Avec la participation de: Regione Friuli Venezia Giulia; Comune di Udine; Accademia Nazionale d'Arte Drammatica "Silvio d'Amico"; Short Theatre; Teatro di Roma (Italie); Théâtre de Liège — Centre européen de création théâtrale et chorégraphique; Centre des Arts scéniques; Théâtre la Balsamine; Ministère de la Communauté française — Service général des Arts de la scène; Wallonie-Bruxelles International (CFWB/Belgique); Ministère de la Culture et de la Communication, Fonds d'Assurance Formation des Activités du Spectacle (France); Universidade de Coimbra et DGArtes — Governo de Portugal/Secretário de Estado da Cultura (Portugal).
{: .production}



<!-- <div class="galerie" markdown=true>
![l'école des maîtres](/images/shows/10-Ecole-des-Maitres/alambic-3.png){: .image-process-large}

![l'école des maîtres](/images/shows/10-Ecole-des-Maitres/calculette.png){: .image-process-large}

![l'école des maîtres](/images/shows/10-Ecole-des-Maitres/banknote-bake-tex.png){: .image-process-large}
</div> -->
</div>
