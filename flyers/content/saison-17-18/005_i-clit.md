<div class="article" markdown=true>



# i-clit 

<div class="push"></div>

## i-clit 

Création danse<br/>
Du 27 février au 3 mars 2018 à 20h30 <br/>
Dans le cadre de Brussels, dance&nbsp;! Focus on contemporary dance.
{: .informations}

Un spectacle manifeste du corps, de la chair et où l'objet sexuel devient sujet.
Une nouvelle vague féministe est née – ultra-connectée, ultra-sexuée et plus populaire. Mais face au pouvoir ambivalent de la pop culture, où et comment se placer dans ce combat en tant que jeune femme&nbsp;? Quelles armes utiliser&nbsp;? 
**i-clit** traque ces moments de fragilité, où l’on oscille entre nouvelles formes d’oppressions et affranchissement.
{: .intro}

A performance manifesto of the body in the flesh and in which the sexual object becomes the subject.
A new wave of feminism was born – ultra-connected, ultra-sexual and more popular. But facing the ambivalent power of pop culture, where and how to position yourself in this battle as a young woman? What weapons should be used?
**i-clit** tracks these moments of fragility, where we oscillate between new forms of oppressions and empowerment.
{: .intro .en}

Concept, chorégraphie, interprétation 
:    Mercedes Dassy

Dramaturgie, regard extérieur 
:    Sabine <br/>Cmelniski

Création lumière, scénographie
:    Caroline Mathieu

Costumes, scénographie 
:    Justine Denos, Mercedes Dassy

Création sonore 
:    Clément Braive

Diffusion 
:    Art Management Agency (AMA)

Production déléguée 
:    Théâtre la Balsamine

Une coproduction du Théâtre la Balsamine avec les soutiens de la Fédération Wallonie-Bruxelles -  <br/>Service de la Danse, du Théâtre Océan Nord, de l’Escaut, du B.A.M.P., de Project(ion) Room et de Friends with benefits. Le son de ce spectacle a été créé grâce au soutien de la SACD.
{: .production }


<div class="push"></div>

Retrouvez l’autocollant complémentaire dans le programme de saison et apposez-le !
{: .autocollant}
