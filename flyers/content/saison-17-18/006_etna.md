<div class="article" markdown=true>



# Etna

<div class="push"></div>

## Etna

Création danse <br>
Les 7 et 8 mars 2018 à 20h30<br/>
Dans le cadre de Brussels, dance&nbsp;! Focus on contemporary dance.
{: .informations}

Une femme errante et solitaire, tel un spectre hanté par ses débris de vie.
**Etna** est une femme sans âge, sans domicile, harcelée par&nbsp;les sonorités de sa vie passée et dont le corps incarne une lassitude extrême. Elle squatte la scène du théâtre et&nbsp;l’habite au travers de ses obsessions et de ses souvenirs. <br/>**Etna** danse pour conjurer le sort et la malchance, pour retenir encore un peu en elle&nbsp;la chaleur du vivant. Elle danse, car sous les oripeaux perdure le désir de sentir, d'être et d'aimer.
{: .intro}

A wandering and solitary woman, like a ghost haunted by the debris of her life.
**Etna** is an ageless, homeless woman, harassed by the sounds of her past life and whose body embodies an extreme lassitude. She squats the stage of the theatre and inhabits it through her obsessions and her memories.
{: .intro .en}

Chorégraphie et interprétation
:    Thi-Mai Nguyen

Création lumières
:    Rémy Urbain<br/>

Son
:    Antoine Delagoutte

Une production de Thi-Mai Nguyen avec le soutien du Théâtre la Balsamine et l’aide de la Maison <br/>de la création, Ultima Vez et du Théâtre Marni.
{: .production }


<div class="push"></div>

Retrouvez l’autocollant complémentaire dans le programme de saison et apposez-le !
{: .autocollant}
