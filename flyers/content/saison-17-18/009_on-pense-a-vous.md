<div class="article" markdown=true>



# La lettre volée

<div class="push"></div>

La lettre volée
{: .paddingmoins}

Denis Bosse/​Pascal Nottet/​Thomas Van Haeperen/​Frédéric Dussenne  
Création Vidéopéra    
Du 9 au 11 novembre 2017 à 20h30  
En partenariat avec Ars Musica.  
{: .informations}

Une enquête palpitante à la recherche d’une lettre perdue. Lyrique et
atypique.
{: .intro}

**La lettre volée**, opéra librement inspiré de la nouvelle éponyme
d’Edgar Poe, convoque à la fois public, chanteurs et musiciens au cœur
d’une action dramatique : la poursuite d’une lettre volée qui rend fous
ceux qui la désirent. Une vertigineuse, dramatique, musicale et
existentielle mise en abîme.
{: .intro}

Directeur artistique et compositeur 
:    Denis Bosse

Librettistes 
:    Pascal Nottet et Denis Bosse

Directeur musical 
:    Thomas Van Haeperen

Conseiller artistique 
:    Pierre Thomas

Metteur en scène
:    Frédéric Dussenne

Chanteurs 
:    Nicolas Ziélinski (Contre-ténor — Dupin), Sarah Defrise (Soprano — Le fou),  
:    Lorenzo Carola (Ténor — Le préfet de Police), Kris Belligh (Baryton Basse — Ministre),  
:    Anne Matic (Mezzo Soprano — Reine), Thomas Van Caekenberghe (Baryton — Narrateur)  

Musiciens 
:    Ensemble Sturm und Klang sous la direction de Thomas Van
Haeperen avec la participation d’étudiants et professeur d’ART², Maxime
Stasyk (violon 1), Loris Douyez (violon 2), Dominica Eyckmans (alto),
Catherine Lebrun (violoncelle), Natacha Save (contrebasse), Justine
Debeer (clarinette), Amaury Geens (saxophones), SzeFong Yeong (cor),
Jean-Louis Maton (percussions), Olivier Douyez (accordéon), Marion Lambert (piano)

Arts Visuels 
:    Étudiants d'ARTS², Jean-François Octave (direction)

Scénographie et vidéo 
:    Helga Dejaegher

Assistants scénographie et vidéo 
:    Dimitri Baheux, Emmanuel Selva

Une production Quart de Ton — Janine Al-Asswad en coproduction avec Ars Musica. Avec les soutiens de : Théâtre la Balsamine, Fédération Wallonie-Bruxelles — conseil de la musique contemporaine, Sturm und Klang, Forum des compositeurs et Festival Loop. En collaboration avec ARTS², École Supérieure des Arts de Mons (Domaine Arts visuels — Musique et Théâtre).
{: .production }

<!-- <div class="display:none;">
logos suivants : fédération, WBI, WBTD, Cocof, culture.be, RAB,
Article 27, arsène 50, Ars Musica
</div> -->
