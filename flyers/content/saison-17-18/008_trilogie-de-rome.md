<div class="article" markdown=true>



# Trilogie de Rome

<div class="push"></div>

## Trilogie de Rome

Création théâtre    
Du 23 au 27 avril 2018 à 20h30   
{: .informations}

On dit que César dans sa jeunesse ayant vu une statue d'Alex- andre le Grand s'était mis à pleurer en répétant&nbsp;: «&nbsp;À mon âge il avait déjà conquis la moitié du monde&nbsp;». Pauvre César qui se&nbsp;plaignait de n'avoir rien fait. Que dire de nous alors&nbsp;?
{: .intro}

Nous sommes en 2018 et nous vivons toujours au temps de César. Les&nbsp;fils du XXI<sup>e</sup> siècle ont quitté leurs couffins. Ils ont désormais du poil au menton et, pour certains, la furieuse envie d'en découdre sur les sables mouvants d'un monde qui n'a pas encore payé le prix de sa légèreté.
{: .intro}

Écriture, mise en scène et dramaturgie 
:    Ludovic Drouet

Avec 
:    Ludovic Drouet, Simon Espalieu, Lucas Meister, Nicolas Patouraux, Andreas Perschewski

Collaboration à l'écriture de plateau et direction d'acteur 
:    Julia Huet-Alberola

Création lumière 
:    Iris Julienne

Création sonore
:    Noam Rzewski

Scénographie 
:    Sébastien Corbière

Costumes 
:    Rita Belova

Une production du Théâtre la Balsamine avec le soutien de la Commission communautaire <br/>française de la Région bruxelloise – Fonds d’Acteurs.
{: .production }

<div class="push"></div>

Retrouvez l’autocollant complémentaire dans le programme de saison et apposez-le !
{: .autocollant}
