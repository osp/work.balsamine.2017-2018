<div class="article" markdown=true>


# Les
# oracles


<div class="push"></div>

## Les oracles

Création transversale <br> danse/​vidéo/​musique/​texte <br>
du 28 au 30 septembre 2017 à 20h30
{: .informations}

**Les oracles** allient danse, poésie, création sonore et vidéo autour
de thématiques à la fois féministes et intemporelles, traversées <br/>par les
textes des écrivaines Catrine Godin et Martine Delvaux.
<br/>La question du genre dans nos perceptions et nos imaginaires
contemporains est ici poétisée. Une proposition artistique
<br/>belgo-québécoise.
{: .intro}

Conception, direction artistique et mise en scène
:    Simon Dumas

Coordination et adminis-<br/>tration du projet
:    Yves Doyon

Conception sonore et coproduction
:    Philippe Franck<br/>

Création vidéo
:    Thomas Israël

<div class="push"></div>

### Percées

Auteure
:    Catrine Godin <br/>

Chorégraphie
:    Karine Ledoyen <br/>

Interprètes (dans la vidéo)
:    Fabien Piché,
:    Ariane Voineau

<div class="push"></div>

### Prototype Nº1

Auteure
:    Martine Delvaux <br/>

Chorégraphie
:    Manon Oligny <br/>

Interprète et co-chorégraphe
:    Marilyn Daoust

<div class="push"></div>

Une production de Rhizome, coproduit par Transcultures, avec les
soutiens de Manon fait de la danse, du Théâtre la Balsamine, du Conseil
des Arts du Canada, du Conseil des arts et des lettres du Québec, du
ministère des Relations internationales et Francophonie du Québec et de
WBI.
{: .production }

<div class="push"></div>

Retrouvez l’autocollant complémentaire dans le programme de saison et apposez-le !
{: .autocollant}
