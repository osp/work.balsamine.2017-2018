<div class="article" markdown=true>


# Les fortunes de la viande


<div class="push"></div>

## Les fortunes de la viande

Création théâtre<br/>
Du 30 janvier au 10 février 2018<br/>
à 20h30 (relâche le dimanche)
{: .informations}


**Les fortunes de la viande** narrent les tribulations existentielles d’un boucher-charcutier, de sa femme et du confesseur de celle-ci, que rejoint à l’impromptu Diane Chasseresse, déboulée pour ainsi dire dans ce marasme métaphysico-trivial. Au fil de ce texte épique, où s’interpénètrent épisodes quotidiens des plus triviaux et mouvements de l’âme parfaitement irrationnels, c’est la métaphysique obscure du quotidien qui est en action. Car les personnages aspirent invariablement au chaos, mus par une volonté d’incorrection absolue visant à briser tous les canons sociétaux.
{: .intro}

Écriture et mise en scène
:    Martine Wijckaert

Assistante à la mise en scène
:    Astrid Howard<br/>

Avec
:    Marie Bos, Héloïse Jadoul, Claude Schmitz, Alexandre Trocki

Scénographie
:    Valérie Jung

Lumières
:    Stéphanie Daniel

Costumes et accessoires
:    Laurence Villerot

Image vidéo
:    Jacques André

Création son
:    Thomas Turine

Direction technique
:    Fred Op de Beeck<br/>

Régie spectacle
:    Mathieu Bastyns

Une production du Théâtre la Balsamine en coproduction avec La Coop asbl, avec le soutien <br/>de Shelterprod, Taxshelter.be, ING et du Tax-Shelter du gouvernement fédéral belge.
{: .production }


<div class="push"></div>

Retrouvez l’autocollant complémentaire dans le programme de saison et apposez-le !
{: .autocollant}
