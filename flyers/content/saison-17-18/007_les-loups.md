<div class="article" markdown=true>



# Les loups

<div class="push"></div>

## Les loups

Création théâtre    
Du 26 au 30 mars 2018 à 20h30   
{: .informations}

Au beau milieu des neiges de l’Antarctique, trois jeunes <br/>biologistes étudient des bactéries inconnues. Ils s’éloignent <br/>de leur campement afin d’élargir leur champ de recherche <br/>et se perdent physiquement et mentalement. L’espace <br/>du désert blanc révèle brutalement leur animalité enfouie, <br/>leur esprit de meute refoulé. Dans **Les loups**, on parlera <br/>de survie, de solidarité entre les espèces et on verra un manchot solitaire jouant du tambour. Ahououou, par avance nous hurlons d’impatience&nbsp;!
{: .intro}

Texte et mise en scène 
:    Jean Le Peltier

Avec 
:    Pierrick De Luca, David Koczij, Jean Le Peltier, Cécile Maidon

Scénographie 
:    Vincent Glowinski

Lumière 
:    Émily Brassier

Costumes
:    Agathe Thomas

Diffusion 
:    Entropie

Regards extérieurs 
:    Vincent Lécuyer, Mohamed Boujarra

Responsable technique
:    Benjamin van Thiel

Une production Ives & Pony en coproduction avec le Théâtre la Balsamine et Le Vivat — Scène conventionnée Danse & Théâtre — Armentières. Production déléguée Théâtre la Balsamine en <br/>collaboration avec Entropie Production.
{: .production }


<div class="push"></div>

Retrouvez l’autocollant complémentaire dans le programme de saison et apposez-le !
{: .autocollant}
