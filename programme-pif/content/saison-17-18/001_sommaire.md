<div class="push"></div>
<div class="edito" markdown=true>

<dl>

<dt>Le 12 juin</dt>
<dd>20h The Ghost Army</dd>
<dd>21h As a Mother of Fact</dd>
</dl>

<dl>
<dt>Le 13 juin</dt>
<dd>20h Nature Morte ou Naturellement Mort</dd>
<dd>21h As a Mother of Fact</dd>
<dd>22h Nature Morte ou Naturellement Mort</dd>
</dl>

<dl>
<dt>Le 14 juin</dt>
<dd>20h Jusqu’à preuve</dd>
<dd>21h Valhalla ou <br>le crépuscule des dieux</dd>
</dl>

<dl>
<dt>Le 15 juin</dt>
<dd>Dès 19h Le Salon</dd>
<dd>20h30 Pattern</dd>
</dl>

<dl>
<dt>Le 16 juin</dt>
<dd>20h Pattern</dd>
<dd>21h De-Siderium</dd>
<dd>22h Brussels Balkan Orchestra<sup>*</sup></dd>
<dd>23h The Ghost Army</dd>
<dd>23h30 Dj Urba<sup>*</sup></dd>
</dl>




<p class="subhead style="margin-top:12pt;"><sup>*</sup>Entrée libre  </p>



</div>
