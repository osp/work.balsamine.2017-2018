<div class="push"></div>
<div class="article" markdown="true">

#De-Siderium {: stroke12}

##Chloé Winkel

Le 16 juin à 21h<br>
Théâtre
{: .informations}

![alt](../../images/show/de-siderium-cut2.png){: .image}

<div class="push"></div>


**De-Siderium** prend racine dans une histoire personnelle, très partagée&nbsp;: la perte d’une grand-mère comme première rencontre avec&nbsp;la mort, opérant une forme de rituel de&nbsp;passage à l’âge adulte. Une famille, ce tout&nbsp;incommensurable, se voit amputée d’un&nbsp;élément fédérateur.
Entremêlant passé, présent et futur, réels et fantasmés et faisant dialoguer les êtres et les choses, cette échappée théâtrale cherche le moyen de sortir d’un système aveugle, d’une histoire &nbsp;aliénante. Sur fond de musique balkanique, &nbsp;&nbsp;&nbsp;la force des corps et des images parlent là &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;où les mots échouent à trouver l’apaisement.
{: .intro}

</div>
