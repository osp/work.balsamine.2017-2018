<div class="push"></div>
<div class="article" markdown="true">

#Nature Morte ou <br>Naturellement Mort {: stroke12}

##Karin Vyncke

Le 13 juin <br>à 20h et 22h<br>
Performance
{: .informations}

<div class="push"></div>


Une performeuse nous invite au voyage par&nbsp;la&nbsp;puissance de l’évocation et peint dans notre imaginaire une toile.
De mot en mot, le&nbsp;spectateur ajoute ses propres touches de couleur et devient le peintre de son propre spectacle.
Le champ s’élargit peu à peu, le&nbsp;peintre déborde du canevas, Cerberus le&nbsp;chien erre.
{: .intro}

![alt](../../images/show/nature-morte-cut02.png){: .image-large}
</div>
