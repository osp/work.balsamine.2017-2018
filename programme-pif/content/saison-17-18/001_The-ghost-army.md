<div class="push"></div>
<div class="article" markdown="true">


#The Ghost Army {: stroke12}

##Boris Dambly / Isabelle Bats

Le 12 juin à 20h <br>et le 16 juin <br>à 23h<br>
Installation – Performance
{: .informations}

![alt](../../images/show/the-ghost-army.cut03.png){: .image}

<div class="push"></div>


La **Ghost Army** était un bataillon des forces alliées pendant la Seconde Guerre mondiale, constituée essentiellement d’artistes recrutés dans le but de mener des opérations de diversion contre les troupes de l’axe.
{: .intro}

Boris Dambly et Isabelle Bats nous proposent de mettre cette stratégie en œuvre afin de faire de la Balsamine une zone souveraine.
{: .intro}

On parlera de lutte poétique, de &nbsp;&nbsp;poésie en acte et, finalement, &nbsp;&nbsp;&nbsp;&nbsp;d’acte de résistance et &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d’insoumission.
{: .intro}

>Nous voulons monter la faction.  
Nous voulons monter les actions.  
Nous voulons commencer l’entraînement.  
Nous voulons répertorier les armes.  
Nous voulons répertorier les âmes.  
Nous voulons mener au désastre.  
Rejoignez la ghost army  
Maintenant !  

</div>
