<div class="push"></div>
<div class="article" markdown="true">

#Jusqu’à preuve {: stroke12}

##Simon Loiseau / Marion Menan

Le 14 juin à 20h<br>
Performance                  
{: .informations}

![alt](../../images/show/jusqua-preuve-feather25.cut01.png){: .image}

<div class="push"></div>


**Jusqu'à preuve** est une performance qui joue avec les concepts scientifiques et les formes plastiques, qui orchestre une rencontre entre théorie et matière. Loin du ton didactique et formel des discours savants, cette expérience appréhende les mots comme des images, et les théories comme des histoires, dans le but de constituer un nouveau récit non linéaire, pour fabriquer une nouvelle manière de raconter le monde.
{: .intro}


</div>
