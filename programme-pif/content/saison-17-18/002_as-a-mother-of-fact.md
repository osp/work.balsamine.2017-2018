<div class="push"></div>
<div class="article" markdown=true>


#As a Mother of Fact {: stroke10}

##notch company / oriane varak

Les 12 et 13 juin <br>à 21h <br>Danse-Théâtre
{: .informations .triche-info}

![alt](../../images/show/as-a-mother-cut.png){: .image}

<div class="push"></div>


**As a Mother of Fact** est un spectacle qui porte sur ces liens conscients et inconscients qui déterminent les relations entre femmes, à travers leur statut de fille, ou de mère.
Il est question d’aliénation, tantôt librement consentie, tantôt subie ou rejetée. Jusqu’où acceptons-nous d’être manipulées&thinsp;?
{: .intro}

Ce projet a été soutenu et développé avec le programme in-Co-laBo mis en place par les ballets C de la B.
{: .production}

</div>
