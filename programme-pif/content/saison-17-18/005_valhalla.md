<div class="push"></div>
<div class="article" markdown="true">

#Valhalla ou le crépuscule <br>des dieux {: stroke10}

##Petri Dish / Anna Nilsson / Sara Lemaire

Le 14 juin <br>à 21h<br>
Cirque - Work <br>in progress
{: .informations}

![alt](../../images/show/valhalla-cut01.png){: .image}

<div class="push"></div>


Un bateau échoué, pris dans l’hiver d’un océan de glace.
À son bord, les&nbsp;derniers survivants d’un monde... sombrant doucement
dans la solitude de la folie. Jusqu’au jour où l’un des membres de l’équipage s’éloigne du bateau... Que la mutinerie commence.
Avec du cirque, chant, danse et cor- nemuse, **Valhalla** parle tout bas du pouvoir, celui qu’on veut, celui qu’on prend, qui s’insinue et que l’on combat, des gens pris dans sa tempête.
{: .intro}

</div>
