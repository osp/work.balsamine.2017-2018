<div class="push"></div>
<div class="article" markdown="true">

#Pattern {: stroke12}

##Emilie Maréchal / Camille Meynard             

Le 15 juin à 20h30 <br>et le 16 juin à 20h<br>
Théâtre <br>documentaire
{: .informations}

![alt](../../images/show/pattern-cut.png){: .image}

<div class="push"></div>


**Pattern** est une expérience scénique et cinématographique qui nous conduit à une&nbsp;réflexion sur la filiation.
Nous traversons le réel pour questionner et regarder le père, notre père.
Cette histoire qui mêle le cinéma documentaire et le théâtre, est entièrement constituée de récits réels.
{: .intro}

Que reste-t-il après la mort d'un père&thinsp;? Il ne reste que la mémoire. 
Un homme est fait de mémoire, s'il n'est pas fait de mémoire, il n'est fait de rien.
{: .intro}

</div>
