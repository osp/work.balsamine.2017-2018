<div class="edito" markdown=true>


#Le Pauvre et Impudent Festival

Aujourd’hui, dans nos villes, la militarisation de l’espace public est devenue la réponse politique pour procurer un&nbsp;sentiment de sécurité. Puisque l’ennemi peut être partout et nulle part à la fois, nous sommes entrés dans&nbsp;une logique guerrière. Qu’en est-il de nos droits fondamentaux&thinsp;? Dans une société de contrôle, l’art <br>peut-il&nbsp;encore se déployer en toute impudence&thinsp;? L’art se&nbsp;révèle une arme non-conventionnelle, elle déstabilise, questionne, réinvente, bouleverse. Pour cette 3<sup>e</sup> édition du PIF, l’artillerie lourde est de sortie. Nous nous con- fronterons à notre propre mort, à celle de nos pères, aux&nbsp;liens maternels qui nous immobilisent à force de bienveillance et d’amour. Comment sortir de ces héritages qui nous écrasent&thinsp;? Comment reformuler clairement nos espérances les plus fondamentales, les plus urgentes et les plus actuelles&thinsp;? Aux arts, citoyens, la vie est à nous.
{: .txtedito1}

<footer>Monica Gomes, direction générale et artistique de la Balsamine.</footer>



</div>
