<div class="push"></div>
<div class="article" markdown="true">

#Le Salon {: stroke12}

##Marine Prunier

Le 15 juin dès 19h<br>Installation – Performance
{: .informations}

![alt](../../images/show/le-salon-cut01.png){: .image}


<div class="push"></div>


Le salon funéraire sert à rendre hommage, à&nbsp;mettre en lumière, une toute dernière fois, le corps du défunt. À cercueil ouvert ou fermé, selon la demande familiale ou l’état du&nbsp;corps. L’espace du salon funéraire est un&nbsp;espace de représentation. Cette mise en&nbsp;scène, à quelques fleurs et rideaux prêts, se répète, répète, répète. À la manière d’un espace de méditation, offrez-vous aujourd’hui la possibilité de faire l’expérience du cercueil en&nbsp;tant qu’objet. 
{: .intro}

</div>
