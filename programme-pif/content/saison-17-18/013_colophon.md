<div class="start-info"></div>
<div class="info-pratique" markdown=true>

#Colophon

##Photos et visuels

Couverture
:   Boris Dambly

The Ghost Army
:   D.R.

As a Mother of Fact
:   Denis Gysen

Nature Morte ou <br>Naturellement Mort
:   Anne-Laure Misme

Jusqu’à preuve
:   Marcelo Mardones

Valhalla ou <br>le crépuscule des dieux
:   Sara Lemaire

Le Salon
:   Marine Prunier

Pattern
:   Camille Meynard

De-Siderium
:   Archives De-Siderium


##Installation graphique  {: .exceptions-info}

**Outils et programmes&thinsp;:**  
html2print, Inkscape, Gimp, git, gitlab, etherpad, GNU/Linux
{: .texteinfo}

**Typographies&thinsp;:**  
Ume stroke stroke condensed, Ume stroke stroke expended
{: .texteinfo}

Fichiers de mise en page disponibles sous licence art libre sur http://osp.kitchen /work/balsamine.2017-2018/
{: .texteinfo}

**Impression&thinsp;:**  
Imprimerie Gillis, Bruxelles.
{: .texteinfo}

**Éditeur responsable&thinsp;:**  
Monica Gomes, <br>Avenue Félix Marchal, 1 <br>1030 Bruxelles
{: .texteinfo}
