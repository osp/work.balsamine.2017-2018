<div class="start-info"></div>
<div class="info-pratique" markdown=true>

#Infos pratiques

##Prix des places

Tarif unique
:   (par performance)

7&thinsp;€
{: .prix}

Tarif soirée combinée
:   (2 performances)

12&thinsp;€
{: .prix}

##Infos et réservations

Par téléphone
:   Au *02 735 64 68*. Du lundi au vendredi de 14h à 18h.

:   En dehors de ces heures et le week-end, un répondeur prend les réservations 24h/24<br>

Par e-mail
:   reservation@balsamine.be

##Bar et restauration

Le bar est ouvert chaque soir 1h avant la première représentation.
Une restauration diversifiée et faite maison vous est proposée.

##Théâtre la Balsamine
Avenue Félix Marchal ,1 – 1030 Bruxelles<br>

Administration
:   02 732 96 18
:   www.balsamine.be
