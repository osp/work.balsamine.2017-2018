# En toute impudence

Nom:

Prénom:

Pass n°:


Ce pass est nominatif et vendu au prix de 20€. La réservation des places peut se faire à tout moment au cours de la saison, sous réserve des places disponibles.

L'espace ci-contre est à faire poinçonner pour activer votre pass.  

Ce passeport vous  donne accès  aux spectacles suivants →




* * *

# L’École des Maîtres 2017

> * date_1:

* 2017/09/11

> * Time:

* 20h00 Entrée libre mais réservation conseillée !

> * Event_type:

* Théâtre

> * Author:

* Albert Dupont

On retourne à l’école avec Transquin, maître quinquin !

??? Transquinquennal




* * *


#Les oracles
* Date: 2017/xx/xx
* Start_date: 2017/09/28
* End_date: 2017/09/30
* Time: 20h30
* Piece_author: Albert Dupont
* Event_type: Création transversale danse / vidéo / musique / texte


> * Direction artistique et mise en scène
> * :    Simon Dumas


## Percées
> * Auteure
> * :    Catrine Godin
> * Chorégraphie
> * :    Karine Ledoyen


##Prototype no. 1
> * Auteure
> * :    Martine Delvaux
> * Chorégraphie
> * :    Manon Oligny



* * *


#Up Pen Down
* Start_date: 2017/10/14
* End_date: 2017/10/14
* Time: 20h30
* Piece_author: OSP
* Event_type: Création transversale danse / vidéo / musique / texte


 (à développer)


* * *

#Lost in Ballets russes
* Date: 2017/xx/xx
* Start_date: 2017/10/24
* End_date: 2017/10/26
* Time: 20h30
* Piece_author: Lara Barsacq
* Event_type: Création danse


> * Conception, texte, dramaturgie et interprétation
> * :    Lara Barsacq


Une production de Gilbert & Stock en coproduction avec Charleroi-Danses, avec les soutiens de la Bellone, de la Raffinerie, de la Ménagerie de verre et du Théâtre la Balsamine.{. production}


* * *

#La lettre volée
* Date: 2017/xx/xx
* Start_date: 2017/11/09
* End_date: 2017/11/11
* Time: 20h30
* Piece_author: En coproduction avec Ars Musica
* Event_type: Vidéopéra


> * Directeur artistique et compositeur
> * :    Denis Bosse



* * *


#Les mardis contemporains
* Date: 2017/xx/xx
* Start_date: 2017/11/14
* End_date: 2017/11/21
* Time: 20h30
* Piece_author: En partenariat avec Ars musica
* Event_type: Musique



##Architecture
* Date2: Le 14 novembre à 20h30


##Cirque
* Date2:  Le 21 novembre à 20h30   




* * *

#Should i stay or should i stay
* Date: 2017/xx/xx
* Start_date: 2017/11/16
* End_date: 2017/11/24
* Time: 20h30
* Piece_author: Simon Thomas
* Event_type: Théâtre

????? Les 16, 20, 22, 23 et 24 Novembre à 20h30


> * Écriture et mise en scène
> * :    Simon Thomas



* * *

#Les Lundis en coulisse
* Date: 2017/xx/xx
* Start_date: 2017/12/11
* End_date: 2017/12/11
* Time: 14h00 à 18h00
* Piece_author:
* Event_type: A la rencontre des écritures dramatiques contemporaines




* * *

#Scripta Manent
* Date: 2017/xx/xx
* Start_date: 2017/02/14
* End_date: 2017/02/15
* Time: 20h30
* Piece_author:
* Event_type: Cycle nouvelles écritures





* * *


#Les carnets de Peter
* Date: 2017/xx/xx
* Start_date: Dates et horaire à confirmer
* End_date: 2017/xx/xx
* Time: 20h30 (relâche le dimanche)
* Piece_author: Le théâtre du Tilleul
* Event_type: Théâtre pour tous, à partir de 7 ans

> * Mise en scène
> * :   Sabine Durand



* * *

#Les fortunes de la viande
* Date: 2017/xx/xx
* Start_date: 2017/01/30
* End_date: 2017/02/10
* Time: 20h30 (relâche le dimanche)
* Piece_author: Martine Wijckaert
* Event_type: Création théâtre


> * ÉCRITURE ET MISE EN SCÈNE
> * :   Martine Wijckaert



* * *

#i-clit
* Date: 2017/xx/xx
* Start_date: 2017/02/27
* End_date: 2017/02/28
* Time: 20h30
* Piece_author: Mercedes Dassy
* Event_type: Création danse


??? Dans le cadre de Brussels, dance ! Focus on contemporary dance.


> * Concept, chorégraphie, interprétation
> * :   Mercedes Dassy



* * *


#Etna
* Date: 2017/xx/xx
* Start_date: 2017/03/07
* End_date: 2017/03/08
* Time: 20h30
* Piece_author: Thi-Mai Nguyen
* Event_type: Création danse

???Dans le cadre de Brussels, dance ! Focus on contemporary dance.


* * *
#Les loups
* Date: 2017/xx/xx
* Start_date: 2017/03/26
* End_date: 2017/03/30
* Time: 20h30
* Piece_author: Jean Le Peltier
* Event_type: Création théâtre



> * Texte et mise en scène
> * :   Jean Le Peltier


* * *

#Trilogie de Rome
* Date: 2017/xx/xx
* Start_date: 2017/04/23
* End_date: 2017/04/27
* Time: 20h30
* Piece_author: Théâtre de Galafronie
* Event_type: Création théâtre


> * Texte et mise en scène
> * :   Ludovic Drouet


Une production de Ludovic Drouet en coproduction avec le Théâtre la Balsamine.{. production}


* * *

#On pense à vous
* Date: 2017/xx/xx
* Start_date: 2017/05/04
* End_date: 2017/05/05
* Time: xxhxx
* Piece_author: Théâtre de Galafronie
* Event_type: Théâtre pour tous à partir de 6 ans


Texte et interprétation
:   Marianne Hansé
Mise en scène
:   Didier de Neck



* * *


#Kunstenfestivaldesarts
Date: 2017/xx/xx
???Start_date: 2017/05/01
???End_date: 2017/05/31


* * *

#PIF 3 - Pauvre et Informe Festival 3
Date: 2017/xx/xx
Start_date: 2017/06/21
End_date: 2017/06/23
Time: 20h30

## Blair pitch projet
