# En toute impudence

En toute impudence
 
Appel à la vivacité, à l’effronterie du rêve, de l’imaginaire, de la pensée vagabonde.
Appel à l’insolence de l’exigence, de l’engagement, du risque et du partage.

En toute imprudence.
Appel aux risques, aux sens, aux plaisirs.
 
Traçons une ligne claire, frondeuse, empathique, audacieuse, décomplexée.

Ne cherchons pas à cacher notre complexité, elle est belle, à l’image de notre humanité.

La scène et sa géographie, son temps, ses possibles…comment transmettre et communiquer la passion de ce qui nous anime dans nos quotidiens virtuellement connectés.

La Balsamine change de dimension.

Elle s’invite d’abord dans vos smartphones, dans vos portables.

La Balsa, version mobile, est une porte d’entrée collective où tout le monde peut s’engouffrer, où la parole est réduite à sa plus simple expression, où l’image est sculptée, directe, ludique. Sorte de partie visible de l’iceberg. 

Le site de la  Balsamine est telle une boîte de pandore, tout y est, mais dans une apparente simplicité.
Enfin le programme papier sera une extraction singulière de l’interface numérique, un objet réflexif, graphique et spontané attaché à donner corps à cette nouvelle saison. 
