#! /bin/bash

Folder=$1

for mdfile in $Folder/*.docx; do
    bash to_md.sh "${mdfile}"
done;