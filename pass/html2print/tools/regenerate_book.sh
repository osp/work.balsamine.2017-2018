#! /bin/bash
book="book${1}"
repofolder=../../The-Riddle
storiesfolder=../stories
chaptersfolder=../chapters

# remove old chapters and stories
echo "Removing previous chapters and stories"
rm $storiesfolder/$book/*.html
for chapterfile in $chaptersfolder/$book/*.html; do
  if [ "${chapterfile}" != "${chaptersfolder}/${book}/template.html" ] && [ "${chapterfile}" != "${chaptersfolder}/${book}/index.html" ] && [ "${chapterfile}" != "${chaptersfolder}/${book}/nameindex.html" ] && [ "${chapterfile}" != "${chaptersfolder}/${book}/text-index.html" ] && [ "${chapterfile}" != "${chaptersfolder}/${book}/flatindex.html" ] && [ "${chapterfile}" != "${chaptersfolder}/${book}/text-nameindex.html" ] && [ "${chapterfile}" != "${chaptersfolder}/${book}/3-all.html" ]; then
    rm "${chapterfile}"
  fi
done

bash fetch_stories.sh $repofolder $storiesfolder
bash make_chapters.sh $storiesfolder $chaptersfolder

python big_break_paragraphs.py $storiesfolder
# python reintroduce_image_references.py $storiesfolder
python modify_images.py $storiesfolder
python wrap_last_sentence_paragraphs.py $storiesfolder/$book
python transform_keywords.py $storiesfolder
python transform_names.py $storiesfolder

bash build_src.sh $chaptersfolder ../js/src.js