#! /bin/bash

In=$1

if [ "$#" -gt 1 ]; then
    Out=$2
else
    Out="${In%.*}.md"
fi

echo "Converting ${In}"
pandoc "${In}" -r docx -t markdown -s -o "${Out}"
echo "Written to: ${Out}"