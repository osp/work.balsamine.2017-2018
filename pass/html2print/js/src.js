;(function(undefined) {
    'use strict';
    var src = {
        "Ecole_des_maitres_2017": "chapters/book0/Ecole_des_maitres_2017.html",
        "Etna": "chapters/book0/Etna.html",
        "i clit": "chapters/book0/i-clit.html",
        "info_pass": "chapters/book0/info_pass.html",
        "intro": "chapters/book0/intro.html",
        "Kunstenfestivaldesarts": "chapters/book0/Kunstenfestivaldesarts.html",
        "la_lettre_volee": "chapters/book0/la_lettre_volee.html",
        "les_carnets_de_peter": "chapters/book0/les_carnets_de_peter.html",
        "les_fortunes_de_la_viande": "chapters/book0/les_fortunes_de_la_viande.html",
        "les_loups": "chapters/book0/les_loups.html",
        "les_lundis_en_coulisse": "chapters/book0/les_lundis_en_coulisse.html",
        "les_mardis_contemporains": "chapters/book0/les_mardis_contemporains.html",
        "les_oracles": "chapters/book0/les_oracles.html",
        "lost_in_ballets_russes": "chapters/book0/lost_in_ballets_russes.html",
        "on_pense_a_vous": "chapters/book0/on_pense_a_vous.html",
        "pass content all": "chapters/book0/pass-content-all.html",
        "PIF_3": "chapters/book0/PIF_3.html",
        "should_i_stay": "chapters/book0/should_i_stay.html",
        "trilogie_de_rome": "chapters/book0/trilogie_de_rome.html",
        "up_pen_down": "chapters/book0/up_pen_down.html",
    }
    var docs = new HTML2print.Docs;
    docs.initialize(src);
})();
